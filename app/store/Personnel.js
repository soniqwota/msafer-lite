Ext.define('lite.store.Personnel', {
    extend: 'Ext.data.Store',

    alias: 'store.personnel',
    storeId: 'personnel',
    autoLoad : true,
    autoSync : true,

    fields: [
        'tipe', 'nominal'
    ],

    proxy: {
        type: 'jsonp',
        api :{
            read :"http://localhost/msafer/read_msafer.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
