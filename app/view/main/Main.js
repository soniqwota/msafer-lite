/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
 Ext.define('lite.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
    'Ext.MessageBox',

    'lite.view.main.MainController',
    'lite.view.main.MainModel',
    'lite.view.view.DataView',
    'lite.view.view.Rincian',
    'lite.view.form.Inputan'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'top',

    items: [
    {
        title: 'Home',
        items: [
        {
            xtype: 'panel',
            title: 'Laporan Keuangan',
            layout: 'fit',
            items:[
            {
                xtype: "bscdataview",   
            }]
        }]
    },{
        title: 'Inputan',
        layout: 'fit',
            items: [{
                xtype: 'inputan'
            }]
        },{
            title: 'Rincian',
            layout: 'fit',
            items: [{
                xtype: 'rincian'
            }]
        },{
            title: 'Info',
            bind: {
                html: '{loremIpsum}'
            }
        }
        ]
    });
