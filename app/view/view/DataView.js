Ext.define("lite.view.view.DataView", {
  extend: "Ext.Container",
  xtype: "bscdataview",

  requires: [
  "Ext.dataview.plugin.ItemTip",
  "Ext.plugin.Responsive",
  "lite.store.Personnel",
  'Ext.field.Search',
  ],
  
  viewModel: {
    stores: {
      personnel: {
        type: "personnel",
      },
    },
  },

  layout: "fit",
  cls: "ks-basic demo-solid-background",
  items: [
  {

    xtype: "dataview",
    scrollable: "y",
    cls: "dataview-basic",
    itemTpl:
    '<tr>'+
    '<td align="left" > <font size="5">Pendapatan</font></td>'+
    '<td align="left" width="20px"> <font size="5">------------</font></td>'+   
    '<td align="left" > <font size="5">{pendapatan}</font></td>'+    //tipe
    '</tr><br><br><br>'+    
    '<tr>'+
    '<td align="left" > <font size="5">Pengeluaran</font></td>'+
    '<td align="left" width="20px"> <font size="5">------------</font></td>'+   
    '<td align="left" > <font size="5">{pengeluaran}</font></td>'+    //tipe
    '</tr><br><br><br>'+    
    '<tr>'+
    '<td align="left" > <font size="5">Balance</font></td>'+
    '<td align="left" width="20px"> <font size="5">------------------</font></td>'+   
    '<td align="left" > <font size="5">{balance}</font></td>'+    //tipe               //nominal                            
    '</tr><br><br><br>',

    bind: {
      store: "{personnel}",
    },

    plugins: {
      type: "dataviewtip",
      align: "l-r?",
      plugins: "responsive",

        // On small form factor, display below.
        responsiveConfig: {
          "width < 600": {
            align: "tl-bl?",
          },
        },
        width: 600,
        minWidth: 300,
        // delegate: "img",
        allowOver: true,
        anchor: true,
        bind: "{record}",
        tpl:
        '<table style="border-spacing:3px;border-collapse:separate">' +
        "<tr><td><font color='blue'>{note} : {nilai}</font></td></tr>" +
        '</table>',
      },
    },

    ],
  });
