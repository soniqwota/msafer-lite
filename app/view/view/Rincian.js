Ext.define('lite.view.view.Rincian', {
    extend: 'Ext.grid.Grid',
    xtype: 'rincian',
    id: 'rincian',

    requires: [
        'lite.store.Histori',
        'Ext.grid.plugin.Editable',
        'lite.view.main.MainController'
    ],

    plugins: [{
        type: 'grideditable'
    }],

    controller: "main",
    bind: '{histori}',


    viewModel: {
    stores: {
      histori: {
        type: "histori",
      },
     },
    },

    columns: [
        //{ text: 'No',  dataIndex: 'no', width: 60 },
        { text: 'Tanggal',  dataIndex: 'tanggal', width: 120 },
        { text: 'Tipe', dataIndex: 'tipe', width: 130},
        { text: 'Nominal', dataIndex: 'nominal', width: 100, editable: true },
        { text: 'Keterangan', dataIndex: 'keterangan', width: 130, editable: true },
        
    ],

  //   listeners:{
  //    select: 'onItemSelected'
  // },
});
